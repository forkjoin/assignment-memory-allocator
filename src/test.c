//
// Created by forkjoin on 12.03.2022.
//

#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static struct block_header *get_header(void *block) {
    return (struct block_header *) ((uint8_t *) block - offsetof(struct block_header, contents));
}

void test__malloc_Given_oneBlock_blockSize30_Expected_nonFree_capacity30(void const *heap) {

    printf("test__malloc_Given_oneBlock_blockSize10_Expected_nonFree_capacity10 test:\n");

    size_t block_size = 30;

    void *bl = _malloc(block_size);
    struct block_header *bh = get_header(bl);

    if (bh->is_free) err("Block allocated, but marked as free!\n");
    if (bh->capacity.bytes != block_size) err("Block allocated with wrong capacity!\n");

    _free(bl);

    debug_heap(stdout, heap);

    printf("test__malloc_Given_oneBlock_blockSize10_Expected_nonFree_capacity10 test passed!\n");
}

void
test__malloc_Given_threeBlocks_blockSize30_Free_thirdBlock_Expected_firstBlock_nonFree_secondBlock_nonFree_thirdBlock_free(
        void const *heap) {
    printf("test__malloc_Given_threeBlocks_blockSize10_Free_thirdBlock_Expected_firstBlock_nonFree_secondBlock_nonFree_thirdBlock_free:\n");

    size_t block_size = 30;

    void *bl1 = _malloc(block_size);
    void *bl2 = _malloc(block_size);
    void *bl3 = _malloc(block_size);

    if (NULL == bl1 || NULL == bl2 || NULL == bl3) err("Memory wasn't allocated!\n");

    debug_heap(stdout, heap);

    _free(bl3);

    debug_heap(stdout, heap);

    if (get_header(bl1)->capacity.bytes != block_size) err("Block allocated with wrong capacity!\n");

    if (get_header(bl1)->is_free) err("Wrong block was freed!\n");
    if (get_header(bl2)->is_free) err("Wrong block was freed!\n");
    if (!get_header(bl3)->is_free) err("Block should be freed!\n");

    _free(bl1);
    _free(bl2);

    printf("test__malloc_Given_threeBlocks_blockSize10_Free_thirdBlock_Expected_firstBlock_nonFree_secondBlock_nonFree_thirdBlock_free passed!\n");
}

void
test__malloc_Given_threeBlocks_blockSize20_30_35_Free_secondBlock_thirdBlock_Expected_firstBlock_nonFree_secondBlock_free_thirdBlock_free(
        void const *heap) {
    printf("test__malloc_Given_threeBlocks_blockSize20_30_35_Free_secondBlock_thirdBlock_Expected_firstBlock_nonFree_secondBlock_free_thirdBlock_free:\n");

    void *bl1 = _malloc(20);
    void *bl2 = _malloc(30);
    void *bl3 = _malloc(35);

    if (NULL == bl1 || NULL == bl2 || NULL == bl3) err("Memory wasn't allocated!\n");

    debug_heap(stdout, heap);

    _free(bl2);
    _free(bl3);

    if (get_header(bl1)->is_free) err("Wrong block was freed!\n");
    if (!get_header(bl2)->is_free) err("Block should be freed!\n");
    if (!get_header(bl3)->is_free) err("Block should be freed!\n");

    _free(bl1);

    printf("test__malloc_Given_threeBlocks_blockSize20_30_35_Free_secondBlock_thirdBlock_Expected_firstBlock_nonFree_secondBlock_free_thirdBlock_free passed!\n");
}

void
test__malloc_Given_oneBlock_blockSize5heapSize_Expected_nonFree_capacity5heapSize(void const *heap, size_t heap_size) {
    printf("test__malloc_Given_oneBlock_blockSize5heapSize_Expected_nonFree_capacity5heapSize:\n");

    void *bl = _malloc(heap_size * 5);

    if (NULL == bl) err("Memory wasn't allocated!\n");

    debug_heap(stdout, heap);

    struct block_header *block_header = get_header(bl);

    if (block_header->is_free) err("Block shouldn't be freed!\n");
    if (block_header->capacity.bytes != heap_size * 5) err("Block allocated with wrong capacity!\n");

    printf("test__malloc_Given_oneBlock_blockSize5heapSize_Expected_nonFree_capacity5heapSize passed!\n");
}
